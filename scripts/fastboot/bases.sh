#!/bin/sh

# Flash both base images to both slots.
fastboot set_active a
fastboot flash bootloader bootloader-taimen-TMZ30m.img
fastboot reboot-bootloader
sleep 5
fastboot flash radio radio-taimen-g8998-00023-2004070200.img
fastboot reboot-bootloader
sleep 5
fastboot set_active b
fastboot flash bootloader bootloader-taimen-TMZ30m.img
fastboot reboot-bootloader
sleep 5
fastboot flash radio radio-taimen-g8998-00023-2004070200.img
fastboot reboot-bootloader
