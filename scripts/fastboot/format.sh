#!/bin/sh

# Proper wipe the Pixel 2 XL before flashing, because Google.
adb reboot bootloader
sleep 7
fastboot set_active a
fastboot format vendor
sleep 3
fastboot format system
sleep 3
fastboot set_active b
fastboot format vendor
sleep 3
fastboot format system
fastboot reboot bootloader