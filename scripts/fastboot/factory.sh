#!/bin/sh

# Replace secureboot key and flash factory image.
fastboot erase avb_custom_key
fastboot flash avb_custom_key avb_pkmd.bin
fastboot reboot-bootloader
sleep 5
fastboot set_active a
fastboot -w --skip-reboot update image-taimen-qq3a.200805.001.zip
fastboot reboot-bootloader
sleep 5
